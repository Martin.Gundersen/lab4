package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    
    /** Amount of rows in the grid */
    int rows;
    /** Amount of columns in the grid */
    int cols;
    /** The grid holding the cellstate */
    CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;

        grid = new CellState[rows][cols];

        for(int row = 0; row<rows;row++){
            for(int col = 0; col<cols;col++){
                grid[row][col]=initialState;
        
            }        
        }


	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        grid[row][column]=element;
        
    }

    @Override
    public CellState get(int row, int column) {
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid result = new CellGrid(this.rows,this.cols,get(0,0));
        
        for(int row = 0; row<rows;row++){
            for(int col = 0; col<cols;col++){
                result.set(row,col,get(row,col));
        
            }        
        }

        return result;
    }
    
}
